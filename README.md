== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version => ruby-2.2.4 

* Rails version => rails-4.2.5

* System dependencies

* Database => Postgres

* Configuration => set your credential in database.yml file 

* Database creation, Database migration 

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* Project setup => Set ruby and rails version 
				=> bundle install
				=> rake db:create
				=> rake db:migrate
				=> rails server

* Git setup => Update change on git repository file
				-git status 
				-git add [file_name]
				-git commit -m "message"
				-git push origin [branch_name]


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.


Inroduction of app

* User can Sign Up with first_name, middle_name, last_name, email address, password and phone 

* Then user can sign in with email and password

* Edit profile

* Registration Confirmation Email

* User can Reset password

* Forget password

class AddSurnameToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :middle_name, :string
  	add_column :users, :phone, :integer, limit: 8
  end
end
